/*
 * adc.h
 *
 *  Created on: Aug 9, 2013
 *      Author: lukas
 */

#ifndef ADC_H_
#define ADC_H_

void init_adc(void)
{
	ADCSRA 	= _BV(ADPS2)  | _BV(ADPS1) | _BV(ADEN);
	ADMUX 	=  _BV(REFS0);
}

#endif /* ADC_H_ */
