/*
 * io_config.h
 *
 *  Created on: Aug 9, 2013
 *      Author: lukas
 */

#ifndef IO_CONFIG_H_
#define IO_CONFIG_H_

//#define F_CPU 8000000UL

#define INPUT 0
#define OUTPUT 1

typedef struct
{
 	unsigned int bit0:1;
	unsigned int bit1:1;
	unsigned int bit2:1;
	unsigned int bit3:1;
	unsigned int bit4:1;
	unsigned int bit5:1;
	unsigned int bit6:1;
	unsigned int bit7:1;
} _io_reg;

#define REGISTER_BIT(rg,bt) ((volatile _io_reg*)&rg)->bit##bt

// ustawienie pinow

#define LED1		REGISTER_BIT(PORTC,5)
#define LED1_DIR	REGISTER_BIT(DDRC,5)

#define LED2		REGISTER_BIT(PORTB,0)
#define LED2_DIR	REGISTER_BIT(DDRB,0)

#define SW1			REGISTER_BIT(PORTD,6)
#define SW1_DIR		REGISTER_BIT(DDRD,6)
#define SW1_PIN		REGISTER_BIT(PIND,6)

#define SW2			REGISTER_BIT(PORTD,7)
#define SW2_DIR		REGISTER_BIT(DDRD,7)
#define SW2_PIN		REGISTER_BIT(PIND,7)

void init_io(void);

void init_io(void)
{
	LED1_DIR = OUTPUT;
	LED1 = 0;
	LED2_DIR = OUTPUT;
	LED2 = 0;

	SW1_DIR = INPUT;
	SW1 = 1;
	SW2_DIR = INPUT;
	SW2 = 1;

	// *** PRZERWANIE ZEW ***

	PCICR |= _BV(PCIE2);
	PCMSK2 |= _BV(PCINT18);

}

#endif /* IO_CONFIG_H_ */
