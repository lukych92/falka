/*
 * terminal.h
 *
 *  Created on: Sep 21, 2013
 *      Author: lukas
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <avr/io.h>

#define COLOR_BLACK		0
#define COLOR_RED		1
#define COLOR_GREEN		2
#define COLOR_YELLOW	3
#define COLOR_BLUE		4
#define COLOR_MAGENTA	5
#define COLOR_CYAN		6
#define	COLOR_WHITE		7

// wymiary terminalu 68x20

void terminal_cls(void);
void terminal_font_color(uint8_t color);
void terminal_background_color(uint8_t color);
void terminal_locate( uint8_t y, uint8_t x );
void terminal_empty_line(uint8_t line);
void terminal_clean(void);
void terminal_empty(void);
void terminal_title(void);
void terminal_mainmenu(void);
void terminal_avoid_obstacle(void);
void terminal_remote_control(void);
void terminal_button(uint8_t y, uint8_t x, uint8_t c);

void terminal_cls(void)
{
	uart_send_string("\x1b""[m");		//atribute off
//	uart_send_string("\x1b""[?25h");	//show cursor
	uart_send_string("\x1b""[?25l");	//hide cursor
	uart_send_string("\x1b""[2J");		//clear screen
	uart_send_string("\x1b""[;H");		//cursor home
}

void terminal_font_color( uint8_t color )
{
	uart_send_string( "\x1b" );
	uart_send_char( '[' );
	uart_send_char( '3' );
	uart_send_char( color+'0' );
	uart_send_char( 'm' );
}

void terminal_background_color(uint8_t color)
{
	uart_send_string( "\x1b" );
	uart_send_char( '[' );
	uart_send_char( '4' );
	uart_send_char( color+'0' );
	uart_send_char( 'm' );
}

void terminal_locate( uint8_t y, uint8_t x )
{
	uart_send_string( "\x1b" );	// <ESC>[y;xH
	uart_send_char( '[' );
	uart_send_number( y );
	uart_send_char( ';' );
	uart_send_number( x );
	uart_send_char( 'H' );
}

void terminal_empty_line(uint8_t line)
{
	uint8_t i = 0;
	terminal_locate(line,0);
	uart_send_char('\xba');
	for(i=0;i<66;i++)
		uart_send_char(' ');
	uart_send_char('\xba');
}

void terminal_clean(void)
{
	int i,j;

	for(i=2;i<20;i++)
	{
		terminal_locate(i,2);
		for(j=0;j<63;j++)
			uart_send_char(' ');
	}
}

void terminal_empty(void)
{
	int i=0;
	terminal_cls();
	terminal_font_color(COLOR_WHITE);
	terminal_background_color(COLOR_MAGENTA);

	terminal_locate(1,1);
	uart_send_char('\xc9');
	for(i=0;i<66;i++) uart_send_char('\xcd');
	uart_send_char('\xbb');

	for(i=2;i<20;i++)
		terminal_empty_line(i);

	terminal_locate(20,0);
	uart_send_char('\xc8');
	for(i=0;i<66;i++) uart_send_char('\xcd');
	uart_send_char('\xbc');
}

void terminal_title(void)
{
	int i=0,k=22;
	for(i=0;i<5;i++)
	{
		terminal_locate(3+i,k);
		if(i==0)	uart_send_string("\xba    \xba");
		if(i==1)	uart_send_string("\xba    \xba");
		if(i==2)	uart_send_string("\xba    \xba");
		if(i==3)	uart_send_string("\xba \xc9\xbb \xba");
		if(i==4)	uart_send_string("\xc8\xcd\xbc\xc8\xcd\xbc");
	}
	for(i=0;i<5;i++)
	{
		terminal_locate(3+i,k+7);
		if(i==0)	uart_send_string("");
		if(i==1)	uart_send_string("");
		if(i==2)	uart_send_string("\xc9\xcd\xcd\xbb");
		if(i==3)	uart_send_string("\xba  \xba");
		if(i==4)	uart_send_string("\xc8\xcd\xcd\xca");
	}
	for(i=0;i<5;i++)
	{
		terminal_locate(3+i,k+12);
		if(i==0)	uart_send_string("\xba \xba");
		if(i==1)	uart_send_string("\xba \xba");
		if(i==2)	uart_send_string("\xba \xba");
		if(i==3)	uart_send_string("\xba \xba");
		if(i==4)	uart_send_string("\xc8\xcd\xc8\xcd");
	}

	for(i=0;i<5;i++)
	{
		terminal_locate(3+i,k+17);
		if(i==0)	uart_send_string("");
		if(i==1)	uart_send_string("");
		if(i==2)	uart_send_string("\xcd\xcd");
		if(i==3)	uart_send_string("");
		if(i==4)	uart_send_string("");
	}
	for(i=0;i<5;i++)
	{
		terminal_locate(3+i,k+20);
		if(i==0)	uart_send_string("");
		if(i==1)	uart_send_string("");
		if(i==2)	uart_send_string("\xc9\xcd\xcd\xbb");
		if(i==3)	uart_send_string("\xcc\xcd\xcd\xbc");
		if(i==4)	uart_send_string("\xc8\xcd\xcd\xcd");
	}
}

void terminal_mainmenu(void)
{
	terminal_empty();
	terminal_font_color(COLOR_WHITE);
	terminal_background_color(COLOR_MAGENTA);

	terminal_title();
	terminal_locate(10,8);
	uart_send_string("Select mode:");
	terminal_locate(12,8);
	uart_send_string("1. Avoiding obstacle");
	terminal_locate(13,8);
	uart_send_string("2. Remote control");
	terminal_locate(14,20);
	uart_send_string("PWML: ");

	terminal_locate(14,40);
	uart_send_string("PWMP: ");
	terminal_locate(14,47);
	uart_send_number(PWMP);
	terminal_locate(14,27);
	uart_send_number(PWML);
	terminal_locate(18,10);
	uart_send_string("Please select number :)");
}

void terminal_avoid_obstacle(void)
{
	terminal_clean();
	terminal_title();

	terminal_locate(8,23);
	uart_send_string("Avoiding obstacle mode");

	terminal_locate(18,10);
	uart_send_string("Press 'm' to back");
}

void terminal_remote_control(void)
{
	terminal_clean();
	terminal_title();

	terminal_locate(8,25);
	uart_send_string("Remote control mode");

	terminal_button(10,15,'w');
	terminal_locate(10,19);
	uart_send_string("GO AHEAD");
	terminal_button(13,15,'s');
	terminal_locate(15,15);
	uart_send_string("GO");
	terminal_locate(16,14);
	uart_send_string("BACK");
	terminal_button(13,10,'a');
	terminal_locate(13,3);
	uart_send_string("TURN");
	terminal_locate(14,3);
	uart_send_string("LEFT");
	terminal_button(13,20,'d');
	terminal_locate(13,24);
	uart_send_string("TURN");
	terminal_locate(14,24);
	uart_send_string("RIGHT");

	terminal_button(10,45,'i');
	terminal_locate(10,49);
	uart_send_string("GO AHEAD SLOWLY");
	terminal_button(13,45,'k');
	terminal_locate(15,45);
	uart_send_string("GO");
	terminal_locate(16,44);
	uart_send_string("BACK");
	terminal_locate(17,43);
	uart_send_string("SLOWLY");
	terminal_button(13,40,'j');
	terminal_locate(13,33);
	uart_send_string("TURN");
	terminal_locate(14,33);
	uart_send_string("LEFT");
	terminal_button(13,50,'l');
	terminal_locate(13,54);
	uart_send_string("TURN");
	terminal_locate(14,54);
	uart_send_string("RIGHT");


	terminal_locate(18,10);
	uart_send_string("Press 'm' to back");
}

void terminal_button(uint8_t y, uint8_t x, uint8_t c)
{
	terminal_locate(y-1,x-2);
	uart_send_string("\xc9\xcd\xcd\xcd\xbb");
	terminal_locate(y,x-2);
	uart_send_string("\xba   \xba");
	terminal_locate(y+1,x-2);
	uart_send_string("\xc8\xcd\xcd\xcd\xbc");
	terminal_locate(y,x);
	uart_send_char(c);
}

#endif /* TERMINAL_H_ */
