/*
 * uart.h
 *
 *  Created on: Jun 29, 2013
 *      Author: lukas
 */

#ifndef UART_H_
#define UART_H_

#include <avr/io.h>
#include <util/delay.h>

void init_uart(unsigned int baud);
uint8_t uart_get_char(void);
void uart_send_char(uint8_t data);
void uart_send_string(const char *s );
void uart_send_number(unsigned long int byte);
int uart_get_number(void);

void init_uart(unsigned int baud)
{
	UBRR0H = (F_CPU/(baud*16L)-1) >> 8; //(unsigned char)(baud>>8);
	UBRR0L = (unsigned char)(F_CPU/(baud*16L)-1); //(unsigned char)baud;

	UCSR0B = (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
	UCSR0C = (3<<UCSZ00);
}

uint8_t uart_get_char(void)
{
	while(!(UCSR0A & (1 << RXC0)));
	return UDR0;
}

void uart_send_char(uint8_t data)
{
	while (!(UCSR0A & (1 << UDRE0)));
	UDR0 = data;
}

void uart_send_string(const char *s )
{
	while (*s) uart_send_char(*s++);
}

void uart_send_number(unsigned long int byte)
{
	unsigned int tmp,ktora_cyfra=0,index_tab=0;
	unsigned char znak, tab[10]={0};

	do
	{
		ktora_cyfra++;
		tmp=byte%10;
		if((!tmp)&&!(ktora_cyfra%2)) {byte/=10; continue;}
		byte-=tmp;
		znak=(char)tmp;
		znak+='0';
		tab[index_tab]=znak;
		index_tab++;
	}
	while(byte);

	ktora_cyfra=(ktora_cyfra/2);
	int m=ktora_cyfra;

	for(m=ktora_cyfra;m>=0;m--)
	{
		while (!(UCSR0A & _BV(UDRE0)));	//Zaczekaj az bufor bedzie pusty
		UDR0=tab[m];
	}
}

int uart_get_number(void)
{
	unsigned char znak;
	int cyfra=0,tab[4]={0};
	int k=0;
	
	do
	{
		znak=uart_get_char();
		switch(znak)
		{
			case 48:
				cyfra = 0;
				break;
			case 49:
				cyfra = 1;
				break;
			case 50:
				cyfra = 2;
				break;		
			case 51:
				cyfra = 3;
				break;		
			case 52:
				cyfra = 4;
				break;		
			case 53:
				cyfra = 5;
				break;		
			case 54:
				cyfra = 6;
				break;		
			case 55:
				cyfra = 7;
				break;		
			case 56:
				cyfra = 8;
				break;		
			case 57:
				cyfra = 9;
				break;
			default:
				break;
		}
		tab[k]=cyfra;
		k++;
	}
	while(znak!='n'&&k<4);
	cyfra=tab[0]*100+tab[1]*10+tab[3];

	return cyfra;
}

#endif /* UART_H_ */
