//#ifndef RC5_H
//#define RC5_H

#define BUZZER_DDR	DDRD
#define BUZZER_PORT	PORTD
#define BUZZER 			PD6

void buzzer_on(void)
{ BUZZER_PORT |= (1<<BUZZER); }

void buzzer_off(void)
{ BUZZER_PORT &= ~(1<<BUZZER); }

void init_buzzer(void)
{
	BUZZER_DDR |= (1<<BUZZER);
	buzzer_off();
}

void buzzer_sound(void)
{
	int i=0;
	for(i=0;i<10;i++)
	{
		buzzer_on();
		_delay_ms(100);
		buzzer_off();
		_delay_ms(5);
	}

}

