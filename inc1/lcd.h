/*---------------------------------------------------------
 Wy�wietlacz alfanumeryczny ze sterownikiem HD44780
 Sterowanie w trybie 4-bitowym bez odczytu flagi zaj�to�ci
 z dowolnym przypisaniem sygna��w steruj�cych
 Plik : HD44780.h	
 Mikrokontroler : Atmel AVR
 Kompilator : avr-gcc
 Autor : Rados�aw Kwiecie�
 �r�d�o : http://radzio.dxp.pl/hd44780/
 Data : 24.03.2007
-------------------------------------------------------------------------------------------------
*/

//-------------------------------------------------------------------------------------------------
// Wy�wietlacz alfanumeryczny ze sterownikiem HD44780
// Sterowanie w trybie 4-bitowym bez odczytu flagi zaj�to�ci
// z dowolnym przypisaniem sygna��w steruj�cych
// Plik : HD44780.c	
// Mikrokontroler : Atmel AVR
// Kompilator : avr-gcc
// Autor : Rados�aw Kwiecie�
// �r�d�o : http://radzio.dxp.pl/hd44780/
// Data : 24.03.2007
//

/*------------------------------------------------------------

 Konfiguracja sygna��w steruj�cych wy�wietlaczem.
 Mo�na zmieni� stosownie do potrzeb.

-------------------------------------------------------------------------------------------------
*/
#define LCD_RS_DIR		DDRC
#define LCD_RS_PORT 	PORTC
#define LCD_RS_PIN		PINC
#define LCD_RS			(1 << PC0)

#define LCD_RW_DIR		DDRC
#define LCD_RW_PORT		PORTC
#define LCD_RW_PIN		PINC
#define LCD_RW			(1 << PC1)

#define LCD_E_DIR		DDRC
#define LCD_E_PORT		PORTC
#define LCD_E_PIN		PINC
#define LCD_E			(1 << PC2)

#define LCD_DB4_DIR		DDRC
#define LCD_DB4_PORT	PORTC
#define LCD_DB4_PIN		PINC
#define LCD_DB4			(1 << PC3)

#define LCD_DB5_DIR		DDRC
#define LCD_DB5_PORT	PORTC
#define LCD_DB5_PIN		PINC
#define LCD_DB5			(1 << PC4)

#define LCD_DB6_DIR		DDRC
#define LCD_DB6_PORT	PORTC
#define LCD_DB6_PIN		PINC
#define LCD_DB6			(1 << PC5)

#define LCD_DB7_DIR		DDRD
#define LCD_DB7_PORT	PORTD
#define LCD_DB7_PIN		PIND
#define LCD_DB7			(1 << PD4)

/*-------------------------------------------------------------

 Instrukcje kontrolera Hitachi HD44780

-------------------------------------------------------------------------------------------------
*/
#define HD44780_CLEAR					0x01

#define HD44780_HOME					0x02

#define HD44780_ENTRY_MODE				0x04
	#define HD44780_EM_SHIFT_CURSOR		0
	#define HD44780_EM_SHIFT_DISPLAY	1
	#define HD44780_EM_DECREMENT		0
	#define HD44780_EM_INCREMENT		2

#define HD44780_DISPLAY_ONOFF			0x08
	#define HD44780_DISPLAY_OFF			0
	#define HD44780_DISPLAY_ON			4
	#define HD44780_CURSOR_OFF			0
	#define HD44780_CURSOR_ON			2
	#define HD44780_CURSOR_NOBLINK		0
	#define HD44780_CURSOR_BLINK		1

#define HD44780_DISPLAY_CURSOR_SHIFT	0x10
	#define HD44780_SHIFT_CURSOR		0
	#define HD44780_SHIFT_DISPLAY		8
	#define HD44780_SHIFT_LEFT			0
	#define HD44780_SHIFT_RIGHT			4

#define HD44780_FUNCTION_SET			0x20
	#define HD44780_FONT5x7				0
	#define HD44780_FONT5x10			4
	#define HD44780_ONE_LINE			0
	#define HD44780_TWO_LINE			8
	#define HD44780_4_BIT				0
	#define HD44780_8_BIT				16

#define HD44780_CGRAM_SET				0x40

#define HD44780_DDRAM_SET				0x80

/*------------------------------------------------------------

 Deklaracje funkcji

-------------------------------------------------------------------------------------------------
*/
void _LCD_OutNibble(unsigned char nibbleToWrite)
{

if(nibbleToWrite & 0x01)
	LCD_DB4_PORT |= LCD_DB4;
else
	LCD_DB4_PORT  &= ~LCD_DB4;

if(nibbleToWrite & 0x02)
	LCD_DB5_PORT |= LCD_DB5;
else
	LCD_DB5_PORT  &= ~LCD_DB5;

if(nibbleToWrite & 0x04)
	LCD_DB6_PORT |= LCD_DB6;
else
	LCD_DB6_PORT  &= ~LCD_DB6;

if(nibbleToWrite & 0x08)
	LCD_DB7_PORT |= LCD_DB7;
else
	LCD_DB7_PORT  &= ~LCD_DB7;
}

unsigned char _LCD_InNibble(void)
{
unsigned char tmp = 0;

if(LCD_DB4_PIN & LCD_DB4)
	tmp |= (1 << 0);
if(LCD_DB5_PIN & LCD_DB5)
	tmp |= (1 << 1);
if(LCD_DB6_PIN & LCD_DB6)
	tmp |= (1 << 2);
if(LCD_DB7_PIN & LCD_DB7)
	tmp |= (1 << 3);
return tmp;
}

unsigned char _LCD_Read(void)
{
unsigned char tmp = 0;
LCD_DB4_DIR &= ~LCD_DB4;
LCD_DB5_DIR &= ~LCD_DB5;
LCD_DB6_DIR &= ~LCD_DB6;
LCD_DB7_DIR &= ~LCD_DB7;

LCD_RW_PORT |= LCD_RW;
LCD_E_PORT |= LCD_E;
tmp |= (_LCD_InNibble() << 4);
LCD_E_PORT &= ~LCD_E;
LCD_E_PORT |= LCD_E;
tmp |= _LCD_InNibble();
LCD_E_PORT &= ~LCD_E;
return tmp;
}


unsigned char LCD_ReadStatus(void)
{
LCD_RS_PORT &= ~LCD_RS;
return _LCD_Read();
}

void _LCD_Write(unsigned char dataToWrite)
{
LCD_DB4_DIR |= LCD_DB4;
LCD_DB5_DIR |= LCD_DB5;
LCD_DB6_DIR |= LCD_DB6;
LCD_DB7_DIR |= LCD_DB7;

LCD_RW_PORT &= ~LCD_RW;
LCD_E_PORT |= LCD_E;
_LCD_OutNibble(dataToWrite >> 4);
LCD_E_PORT &= ~LCD_E;
LCD_E_PORT |= LCD_E;
_LCD_OutNibble(dataToWrite);
LCD_E_PORT &= ~LCD_E;
while(LCD_ReadStatus()&0x80);
}


void LCD_WriteCommand(unsigned char commandToWrite)
{
LCD_RS_PORT &= ~LCD_RS;
_LCD_Write(commandToWrite);
}



void LCD_WriteData(unsigned char dataToWrite)
{
LCD_RS_PORT |= LCD_RS;
_LCD_Write(dataToWrite);
}

unsigned char LCD_ReadData(void)
{
LCD_RS_PORT |= LCD_RS;
return _LCD_Read();
}

void LCD_WriteText(char * text)
{
while(*text)
  LCD_WriteData(*text++);
}

void LCD_GoTo(unsigned char x, unsigned char y)
{
LCD_WriteCommand(HD44780_DDRAM_SET | (x + (0x40 * y)));
}

void LCD_Clear(void)
{
LCD_WriteCommand(HD44780_CLEAR);
_delay_ms(2);
}

void LCD_Home(void)
{
LCD_WriteCommand(HD44780_HOME);
_delay_ms(2);
}

void LCD_Initalize(void)
{
unsigned char i;
LCD_DB4_DIR |= LCD_DB4; // Konfiguracja kierunku pracy wyprowadze�
LCD_DB5_DIR |= LCD_DB5; //
LCD_DB6_DIR |= LCD_DB6; //
LCD_DB7_DIR |= LCD_DB7; //
LCD_E_DIR 	|= LCD_E;   //
LCD_RS_DIR 	|= LCD_RS;  //
LCD_RW_DIR 	|= LCD_RW;  //
_delay_ms(15); // oczekiwanie na ustalibizowanie si� napiecia zasilajacego
LCD_RS_PORT &= ~LCD_RS; // wyzerowanie linii RS
LCD_E_PORT &= ~LCD_E;  // wyzerowanie linii E
LCD_RW_PORT &= ~LCD_RW;
for(i = 0; i < 3; i++) // trzykrotne powt�rzenie bloku instrukcji
  {
  LCD_E_PORT |= LCD_E; //  E = 1
  _LCD_OutNibble(0x03); // tryb 8-bitowy
  LCD_E_PORT &= ~LCD_E; // E = 0
  _delay_ms(5); // czekaj 5ms
  }

LCD_E_PORT |= LCD_E; // E = 1
_LCD_OutNibble(0x02); // tryb 4-bitowy
LCD_E_PORT &= ~LCD_E; // E = 0

_delay_ms(1); // czekaj 1ms 
LCD_WriteCommand(HD44780_FUNCTION_SET | HD44780_FONT5x7 | HD44780_TWO_LINE | HD44780_4_BIT); // interfejs 4-bity, 2-linie, znak 5x7
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_OFF); // wy��czenie wyswietlacza
LCD_WriteCommand(HD44780_CLEAR); // czyszczenie zawartos�i pamieci DDRAM
LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);// inkrementaja adresu i przesuwanie kursora
LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); // w��cz LCD, bez kursora i mrugania
}

void init_lcd(void)
{
	LCD_Initalize();
	LCD_WriteCommand(HD44780_CLEAR);
	LCD_WriteCommand(HD44780_ENTRY_MODE | HD44780_EM_SHIFT_CURSOR | HD44780_EM_INCREMENT);
	LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON | HD44780_CURSOR_OFF | HD44780_CURSOR_NOBLINK); 

	LCD_Clear();
}

void LCD_WriteNumber(unsigned int byte)
{
	unsigned int tmp,ktora_cyfra=0,index_tab=0;
	unsigned char znak, tab[10]={0};

	do
	{
		ktora_cyfra++;
		tmp=byte%10;
		if((!tmp)&&!(ktora_cyfra%2)) {byte/=10; continue;}
		byte-=tmp;
		znak=(char)tmp; 	
		znak+='0';
		tab[index_tab]=znak;
		index_tab++;
	}
	while(byte);

	ktora_cyfra=(ktora_cyfra/2);
	int m=ktora_cyfra;

	for(m=ktora_cyfra;m>=0;m--)	LCD_WriteData(tab[m]);
}

void LCD_WriteChar(uint8_t znak)
{
	
	char i=0;
	i=(char)znak;  
//	i+='0';
	LCD_WriteData(i);
}
/*
-------------------------------------------------------------------------------------------------

 Koniec pliku HD44780.h

-------------------------------------------------------------------------------------------------
*/
