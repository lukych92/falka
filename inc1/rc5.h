//#ifndef RC5_H
//#define RC5_H

volatile unsigned char rc5=0;

void init_rc5()
{

    DDRD  &= ~(1<<PD2); 
    PORTD &= ~(1<<PD2);
    //
    MCUCR |= (1<<ISC01); // opadajace zbocze 
    GICR  |= (1<<INT0);	
    //
    TCCR0 = (1<<CS02); // prescaler 256
    TIFR |= (1<<TOV0); 

/*
  //atmega88
  TCCR0B = (1<<CS00);
  TIMSK0 = (1<<TOIE0);
*/
}

ISR(INT0_vect)
{
   static unsigned char n, pb;
   static unsigned int code;
  
   switch(n)
   {
      case 0:

         if(MCUCR&(1<<ISC00))
         {
            MCUCR &= ~(1<<ISC00);
	    TCNT0 = 0x00; TIFR |= (1<<TOV0);
         }
         else 
         {
	    if((TCNT0>112)||(TIFR&(1<<TOV0))) // 112 = 3,6ms
	    {	    
               pb=0;	    
	       code = 0x03;
               TCNT0  = 0x00; TIFR |= (1<<TOV0);
               n = 1;
	    }
            MCUCR |= (1<<ISC00);
	 }
      break;

      case 1:
      case 2:	
         if((TCNT0>14)&&(TCNT0<42))  // 14 = 0.4ms, 42 = 1.35ms
	 {
            MCUCR ^= (1<<ISC00);
            TCNT0  = 0x00; TIFR |= (1<<TOV0);
	    n++;
	 }
	 else n = 0;

      break;

      default:  
    
         if((TCNT0>14)&&(TCNT0<42))
	 {
             if(!pb)
             {		     
	        pb = 1;
                code <<= 1;
		if(code&0x2) code |= 1;
             }
	     else
             {
                pb = 0;
		n++;
	     }

	     TCNT0 = 0x00; TIFR |= (1<<TOV0);
	 }
	 else if((TCNT0>42)&&(TCNT0<70)) // 70=2.25ms
	 {
             code <<= 1;
	     if(!(code&0x2)) code |= 1;

	     TCNT0 = 0x00; TIFR |= (1<<TOV0);
             n++;
	 }
         else  n = 0;		 

         MCUCR ^= (1<<ISC00);

	 if(n==15)
	 {
	     n = 0;
	     rc5 = code;
	     if(code & (1<<11)) rc5 |= 0x80;
			rc5 &= ~(1<<7);
	 }

      break;
   }
}

