//#ifndef SERWO_H
//#define SERWO_H


void serwo_L(void)
{ OCR2 = 14; } 
//OCR1B = 2000; /*określenie czasu impulsu 2ms*/ }

void serwo_S(void)
{ OCR2 = 10; }
//OCR1B = 1500; /*określenie czasu impulsu 1,5ms*/ }

void serwo_P(void)
{ OCR2 = 6;  }
//OCR1B = 1000; /*określenie czasu impulsu 1ms*/ }

void init_serwo(void)
{

	DDRB |= (1<<PB3);

/*Fast PWM, prescaler 1024, F_CPU 8MHz */
	TCCR2 = (1<<WGM21) | (1<<WGM20) | (1<<COM21) | (1<<CS22) | (1<<CS21) | (1<<CS20);


	/*konfiguracja portow stan wysoki na PB1 */ 
//	DDRB = (1<<PB2); 

	/*tryb pracy licznika(14), ustawienie prescalera na 8, */
//	TCCR1A = (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11); 

	/*ustawienie wyjścia z licznika*/
//	TCCR1B = (1<<CS11)|(1<<WGM12)|(1<<WGM13); 

	/*ustawienie okresu na 20ms(czestotliwosc 50Hz)*/
//	ICR1 = 20000;

	serwo_S();
	serwo_P();
	serwo_L();
	serwo_S();
}

void pomiar_serwo()
{
	serwo_S();
	_delay_ms(2*time_wait);
	serwo_P();
	_delay_ms(2*time_wait);
	serwo_L();
	_delay_ms(2*time_wait);
	serwo_S();
	_delay_ms(2*time_wait);
}

