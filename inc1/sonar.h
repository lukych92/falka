//#ifndef SONAR_H
//#define SONAR_H

volatile unsigned int distance = 0; // w milimetrach
volatile unsigned int time = 0; // w mikrosekundach
volatile unsigned char up = 0; // czy zbocze rosnace
volatile uint32_t timerCounter = 0; //licznik timera
volatile unsigned char running = 0;	// czy zostal uruchomiony pomiar
volatile uint16_t PulseWidth = 0; //licznik timera

void init_sonar(void)
{
	DDRD |= (1<<PD7);       //wyjscie
	PORTD &= ~(1<<PD7); 		//TRIGER

	DDRB &= ~(1<<PB0);      //wejscie ECHO
	PORTB &= ~(1<<PB0);     //ICP
	
	TCCR1B |= (1<<ICES1);  	/* Zbocze narastające wywoła przerwanie*/
	TCCR1B |= (1<<CS10);   	/* preskaler = 8 */
//TCCR1B |= (1<<ICNC1); //NOISE CANCELLER
	TIMSK |=  (1<<TICIE1); 	/* zezwolenie na przerwanie od ICP */
/*	DDRD |= (1<<PD7);       //wyjscie
	PORTD &= ~(1<<PD7); 		//TRIGER

	DDRD &= ~(1<<PD3); //przerwanie int1 wejscie
	PORTD &= ~(1<<PD3);// przerwanie int1 stan niski

	//TIMER0 czas po jakim nastąpi przerwanie 1us
                // Timer0 zlicza sekundy
        // ustawiamy 8 bitowy licznik i właczamy przerwanie od przepełnienia po zliczeniu do 255
	TCCR0 = (1<<CS00);//prescaler 1, F_CPU 8MHz
	TCNT0 = 0; // wartosc timera = 0
	TIMSK = (1<<TOIE0); // włączamy przerwanie timera
 
	//INT1
	//podłączyć ECHO
	GICR |= (1<<INT1);//wlaczamy INT1
	MCUCR |= (1<<ISC10);//kazda zmiana zbocza
*/
}

void trig(void)            
{ 
	PORTD	 &= ~(1<<PD7);
	_delay_us(2);
	PORTD |= (1<<PD7); 			//wystawienie jedynki na TRIG
  running = 1;  					// start
	_delay_us(11);          //wymagane 10 us stanu wysokiego
	PORTD &= ~(1<<PD7);     //wystawienie zera na TRIG
}
/*
ISR(TIMER0_OVF_vect)
{
	if(up) timerCounter++;
}
 
ISR(INT1_vect)
{
	if (running) 
	{   //akceptujemy przerwanie tylko gdy sonar startuje
		if (!up) 
		{ // wzrost napięcia -- start pomiaru czasu
			up = 1;
      timerCounter = 0;
      TCNT0 = 0; // reset licznika
		}
		else
		{  // spadek napiecia - zatrzymanie pomiaru
       up = 0;
      time = (((timerCounter * 256) + TCNT0)/256)*25;
			distance = time/5;
      running = 0;
     }
 }
}
*/ 



ISR(TIMER1_CAPT_vect)
{
	static uint16_t LastCapture;
 
	if( !(TCCR1B & (1<<ICES1)) ) 
		PulseWidth = ICR1 - LastCapture;
	LastCapture = ICR1;
 
	TCCR1B ^= (1<<ICES1); //zmiana zbocza
}

