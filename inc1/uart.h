//#ifndef UART_H
//#define UART_H

static void init_uart(unsigned int baud)
{
	UBRRH = (F_CPU/(baud*16L)-1) >> 8; //(unsigned char)(baud>>8); 
	UBRRL = (unsigned char)(F_CPU/(baud*16L)-1); //(unsigned char)baud; 
	UCSRB = ((1<<RXCIE)|(1<<TXEN)|(1<<RXEN)) ;	//bez parzystosci
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);//8 bitów danych + 2 bity stopu

}

uint8_t uart_rec()
{
	while(!(UCSRA & _BV(RXC)));
	return UDR;
}

void uart_send_char(uint8_t byte)
{
	while (!(UCSRA & _BV(UDRE)));	//Zaczekaj az bufor bedzie pusty
	UDR=byte;
}

void uart_send_string(const char *s )
{
	while (*s) uart_send_char(*s++);
}

void uart_send_number(uint16_t byte)
{
	unsigned int tmp,ktora_cyfra=0,index_tab=0;
	unsigned char znak, tab[10]={0};

	do
	{
		ktora_cyfra++;
		tmp=byte%10;
		if((!tmp)&&!(ktora_cyfra%2)) {byte/=10; continue;}
		byte-=tmp;
		znak=(char)tmp; 	
		znak+='0';
		tab[index_tab]=znak;
		index_tab++;
	}
	while(byte);

	ktora_cyfra=(ktora_cyfra/2);
	int m=ktora_cyfra;

	for(m=ktora_cyfra;m>=0;m--)	
	{	
		while (!(UCSRA & _BV(UDRE)));	//Zaczekaj az bufor bedzie pusty
		UDR=tab[m];
	}
}
