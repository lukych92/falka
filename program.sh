#!/bin/bash

Nazwa=main

#avr-gcc -mmcu=atmega8 -Iinc -Os src/main.c -o obj/main.o
#kompilacja_=$?

avr-gcc -mmcu=atmega88 -Iinc -Os src/$Nazwa.c -o obj/$Nazwa.o 
kompilacja=$?



if [ "$kompilacja" == 0 ]
then
	echo "    Poprawnie skompilowano $Nazwa.c"
	avr-gcc obj/$Nazwa.o -o out/$Nazwa.out
	avr-objcopy -O ihex out/$Nazwa.out hex/$Nazwa.hex
	avrdude -c usbasp -p m88p -P /dev/ttyUSB1  -U flash:w:hex/$Nazwa.hex
else
	echo "    Wystąpił błąd kompilacji $Nazwa.c"
fi

#if [ "$kompilacja_" == 0 ]
#then 
#	echo "    Poprawnie skompilowana main"
#else
#	echo "    main nie zostalo skompilowane"
#fi
