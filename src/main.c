#include <avr/io.h>
#define F_CPU 8000000UL
#include <util/delay.h>
#include <avr/interrupt.h>

//#include <uart.h>
#include <silniki.h>
#include <adc.h>

volatile int jazda = 0,petla=0;

long long int czujniki[5] = {0};
int blad=0, pop_blad=0;
int regulacja=0;
int prev_err =  0;
int Kp=5, Kd=2, waga=10;
int V =120; 
/*
void terminal_locate( uint8_t y, uint8_t x )
{
	uart_send_string( "\x1b" );	// <ESC>[y;xH
	uart_send_char( '[' );
	uart_send_number( y );
	uart_send_char( ';' );
	uart_send_number( x );
	uart_send_char( 'H' );
}

void terminal_cls(void)
{
	uart_send_string("\x1b""[m");		//atribute off
//	uart_send_string("\x1b""[?25h");	//show cursor
	uart_send_string("\x1b""[?25l");	//hide cursor
	uart_send_string("\x1b""[2J");		//clear screen
	uart_send_string("\x1b""[;H");		//cursor home
}
*/
void czytaj_adc() 
{ 
	int i=0;
	for( i=0; i<5; i++) 
	{ 
		ADMUX &= 0b11100000;                      
		ADMUX |= i;                                   
		ADCSRA |= _BV(ADSC);                          
		while(ADCSRA & _BV(ADSC)) {};   
		if(ADC > 512)                                
       	{   
       	   czujniki[i] = 1; 
       	}
   		else 
   		    czujniki[i] = 0; 

	} 
}

int licz_blad() 
{ 
	int err = 0; 
	int ilosc = 0; 
	int i=0;
        
	for(i=0; i<5; i++) 
	{ 
		err += czujniki[i]*(i-2)*waga; 
		ilosc += czujniki[i]; 
	} 
	if(ilosc != 0)	// czujniki wykryły linię 
	{ 
		err /= ilosc;	// liczymy i zapisujemy aktualny błąd 
 		prev_err = err; 
	} 
	else	// czujniki nie wykryły linii 
		err = prev_err;		// używamy zapisanego poprzedniego błędu 
        
	return err;       
}

int PD() 
{ 
	int rozniczka;	
	
	rozniczka = blad - pop_blad; 
	pop_blad = blad; 
	
	return Kp*blad + Kd*rozniczka; 
}

void petla_LF()
{
	LED1=1;
	czytaj_adc();
	blad = licz_blad();
	regulacja = PD();
	PWM(V + regulacja, V - regulacja);
}
/*
void menu()
{
	terminal_locate(0,0);
	int i = 0;
	czytaj_adc();
	for(i=0;i<5;i++)
	{
		uart_send_number(czujniki[i]);
		uart_send_string(" ");
	}
	uart_send_string("\r\n");

	uart_send_string("PWM1: ");
	uart_send_number(OCR1A);
	uart_send_string("\r\n");
	uart_send_string("PWM2: ");
	uart_send_number(OCR1B);
	uart_send_string("\r\n");

	uart_send_string("V: ");
	uart_send_number(V);
	uart_send_string(" ");
	uart_send_string("Kp: ");
	uart_send_number(Kp);
	uart_send_string(" ");	
	uart_send_string("Kd: ");
	uart_send_number(Kd);
	uart_send_string("\r\n");
	uart_send_string("Waga czunikow: ");
	uart_send_number(waga);
	uart_send_string("\r\n\r\nv - ustaw predkosc nominalna\r\n");
	uart_send_string("b - ustaw kp\r\n");
	uart_send_string("n - ustaw kd\r\n");
	uart_send_string("m - ustaw wagi czujnikow\r\n");
	uart_send_string("s - start\r\n");
	_delay_ms(1000);
	terminal_cls();
}
*/
/*
void stan_czujnikow()
{
	int i=0;
	czytaj_adc();
	for(i=0;i<5;i++)
	{
		uart_send_number(czujniki[i]);
		uart_send_string(" ");
	}
	uart_send_string("\r\n");
	_delay_ms(50);
	terminal_cls();
}

void ustaw_predkosc()
{
	uart_send_string("\r\nUstaw predkosc:\r\nV: ");
	V = uart_get_number();
	terminal_cls();
}

void ustaw_kp()
{
	uart_send_string("\r\nKp: ");
	Kp = uart_get_number();
	terminal_cls();
}

void ustaw_kd()
{
	uart_send_string("\r\nKd: ");
	Kd = uart_get_number();
	terminal_cls();
}

void ustaw_wage_czujnikow()
{
	uart_send_string("\r\nWaga czujnikow: ");
	waga = uart_get_number();
	terminal_cls();
}
*/
int main(void)
{
	init_io();
	init_adc();
//	init_uart(19200);
	init_silniki();

	sei(); // globalna zgoda na przerwania

	LED2 = 0;
	jazda = 0;
//	terminal_cls();

	while(1)
	{	

		if(jazda)
			petla_LF();
		else 
		{
//			menu();
			PWM(0,0); 
			LED1=1	;
		}
	}   
	return 0;
}

ISR(BADISR_vect){}

ISR(PCINT2_vect)
{
	PCMSK2 &= ~_BV(PCINT23);	// wylaczamy obsluge przerwania
	_delay_ms(250);
	jazda ^= 1;
	PCMSK2 |= _BV(PCINT23);	// wlaczamy z powrotem obsluge przerwania
}
/*
ISR(USART_RX_vect)
{
	uart_send_string("\x1b""[?25h");	//show cursor
	LED1^=1;
	uint8_t c = uart_get_char();
	if(c == 's') jazda^=1;
	if(c == 'v') ustaw_predkosc();
	if(c == 'b') ustaw_kp();
	if(c == 'n') ustaw_kd();
	if(c == 'm') ustaw_wage_czujnikow();
}
*/
